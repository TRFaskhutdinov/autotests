﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using UnitTestProject.Models;

namespace UnitTestProject
{
    public static class Settings
    {
        public static string File = "Settings.xml";

        #region Fields
        private static string _baseUrl;
        private static string _login;
        private static string _password;
        private static string _userName;
        private static XmlDocument document;
        #endregion

        static Settings()
        {
            //if (!System.IO.File.Exists(File)) { throw new Exception("Problem: settings file not found: " + File); }
            try
            {
                document = new XmlDocument();
                document.Load(@"C:\Source\Tests\autotests\UnitTestProject\UnitTestProject\bin\Debug\Settings.xml");
            }
            catch (Exception)
            {
                throw new Exception("Problem: settings file not found");
            }
        }

        #region Properties
        public static string BaseUrl
        {
            get
            {
                if (_baseUrl == null)
                {
                    XmlNode node = document.DocumentElement.SelectSingleNode("BaseUrl");
                    _baseUrl = node.InnerText;
                }
                return _baseUrl;
            }
        }

        public static string Login
        {
            get
            {
                if (_login == null)
                {
                    XmlNode node = document.DocumentElement.SelectSingleNode("Login");
                    _login = node.InnerText;
                }
                return _login;
            }
        }

        public static string Pass
        {
            get
            {
                if (_password == null)
                {
                    XmlNode node = document.DocumentElement.SelectSingleNode("Pass");
                    _password = node.InnerText;
                }
                return _password;
            }
        }

        public static string UserName
        {
            get
            {
                if (_userName == null)
                {
                    XmlNode node = document.DocumentElement.SelectSingleNode("UserName");
                    _userName = node.InnerText;
                }
                return _userName;
            }
        }
        #endregion

    }
}
