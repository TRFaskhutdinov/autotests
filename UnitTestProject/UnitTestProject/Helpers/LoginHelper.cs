﻿using System;
using System.Threading;
using Microsoft.Win32;
using OpenQA.Selenium;
using UnitTestProject.Bases;
using UnitTestProject.Models;

namespace UnitTestProject.Helpers
{
    internal class LoginHelper : HelperBase
    {
        internal void LogOut()
        {
            if (!IsLoggedIn())
            {
                return;
            }
            Driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Your work'])[1]/preceding::button[1]")).Click();
            Thread.Sleep(Manager.SleepTime);
            try
            {
                Driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Bitbucket Labs'])[1]/following::span[2]")).Click();
            }
            catch (NoSuchElementException)
            {
                Driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Your work'])[1]/preceding::button[1]")).Click();
                Thread.Sleep(Manager.SleepTime);
                Driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Bitbucket Labs'])[1]/following::span[2]")).Click();
            }
            
            Thread.Sleep(Manager.SleepTime);
        }

        internal void LogIn(AccountData user)
        {
            if (IsLoggedIn())
            {
                if (IsLoggedIn(user.Login))
                {
                    return;
                }
                LogOut();
            }

            Manager.Navigation.OpenLoginPage();
            Driver.FindElement(By.Id("username")).Click();
            Thread.Sleep(Manager.SleepTime);
            Driver.FindElement(By.Id("username")).Clear();
            Thread.Sleep(Manager.SleepTime);
            Driver.FindElement(By.Id("username")).SendKeys(user.Login);
            Thread.Sleep(Manager.SleepTime);
            Driver.FindElement(By.Id("login-submit")).Click();
            Thread.Sleep(Manager.SleepTime);
            Driver.FindElement(By.Id("password")).Clear();
            Thread.Sleep(Manager.SleepTime);
            Driver.FindElement(By.Id("password")).SendKeys(user.Pass);
            Thread.Sleep(Manager.SleepTime);
            Driver.FindElement(By.Id("login-submit")).Click();
            Thread.Sleep(30000);
        }

        internal bool IsLoggedIn()
        {
            try
            {
                Driver.FindElement(By.XPath(
                    "(.//*[normalize-space(text()) and normalize-space(.)='Your work'])[1]/preceding::button[1]"));
            }
            catch (NoSuchElementException)
            {
                return false;
            }
            return true;
        }

        internal bool IsLoggedIn(string username)
        {
            try
            {
                Driver.FindElement(By.XPath(
                    "(.//*[normalize-space(text()) and normalize-space(.)='Your work'])[1]/preceding::button[1]")).Click();
                Thread.Sleep(Manager.SleepTime);
                Driver.FindElement(By.XPath($"(.//*[normalize-space(text()) and normalize-space(.)='{username}'])[1]/following::span[2]"));
                Thread.Sleep(Manager.SleepTime);
                Driver.FindElement(By.XPath($"(.//*[normalize-space(text()) and normalize-space(.)='{username}'])[1]/preceding::button[1]")).Click();
                Thread.Sleep(Manager.SleepTime);
            }
            catch (NoSuchElementException)
            {
                return false;
            }
            return true;
        }

        internal LoginHelper(ApplicationManager manager) : base(manager)
        {

        }
    }
}
