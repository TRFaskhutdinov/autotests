﻿using System.Threading;
using UnitTestProject.Bases;

namespace UnitTestProject.Helpers 
{
    internal class NavigationHelper : HelperBase
    {

        internal void OpenHomePage()
        {
            Driver.Navigate().GoToUrl(Manager.BaseUrl);
            Thread.Sleep(Manager.SleepTime);
        }

        internal void OpenLoginPage()
        {
            Driver.Navigate().GoToUrl("https://id.atlassian.com/login?continue=https%3A%2F%2Fid.atlassian.com%2Fopenid%2Fv2%2Fop%3Fopenid.return_to%3Dhttps%3A%2F%2Fbitbucket.org%2Fsocialauth%2Fcomplete%2Fatlassianid%2F%3Fjanrain_nonce%253D2018-11-12T21%25253A19%25253A03ZZREuFx%26openid.sreg.optional%3Dfullname%2Cnickname%2Cemail%26openid.ns%3Dhttp%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%26openid.ns.sreg%3Dhttp%3A%2F%2Fopenid.net%2Fextensions%2Fsreg%2F1.1%26openid.crowdid.application%3Dbitbucket%26openid.assoc_handle%3D11519367%26openid.ns.crowdid%3Dhttps%3A%2F%2Fdeveloper.atlassian.com%2Fdisplay%2FCROWDDEV%2FCrowdID%252BOpenID%252Bextensions%2523CrowdIDOpenIDextensions-login-page-parameters%26openid.identity%3Dhttp%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select%26openid.realm%3Dhttps%3A%2F%2Fbitbucket.org%26openid.claimed_id%3Dhttp%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select%26openid.mode%3Dcheckid_setup&prompt=&application=bitbucket&tenant=&email=&errorCode=");
            Thread.Sleep(Manager.SleepTime);
        }

        internal void OpenRepListPage()
        {
            //Driver.Navigate().GoToUrl("https://bitbucket.org/dashboard/overview");
            Driver.Navigate().GoToUrl(Manager.BaseUrl + "/dashboard/overview");
            Thread.Sleep(Manager.SleepTime);
        }

        internal NavigationHelper(ApplicationManager manager) : base(manager)
        {

        }
    }
}
