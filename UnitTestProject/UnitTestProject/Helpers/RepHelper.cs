﻿using System.Threading;
using OpenQA.Selenium;
using UnitTestProject.Bases;
using UnitTestProject.Models;

namespace UnitTestProject.Helpers
{
    internal class RepHelper : HelperBase
    {
        internal void CreateRep(RepData rep)
        {
            Manager.Navigation.OpenRepListPage();
            Driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Your work'])[1]/preceding::button[3]")).Click();
            Thread.Sleep(Manager.SleepTime);
            Driver.FindElement(By.LinkText("Repository")).Click();
            Thread.Sleep(Manager.SleepTime);
            Driver.FindElement(By.Id("id_name")).SendKeys(rep.Name);
            Thread.Sleep(Manager.SleepTime);
            Driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Select language…'])[1]/following::button[1]")).Click();
            Thread.Sleep(Manager.SleepTime);
        }

        internal void DeleteRep()
        {
            Driver.FindElement(By.LinkText("Settings")).Click();
            Thread.Sleep(Manager.SleepTime);
            Driver.FindElement(By.Id("delete-repo-btn")).Click();
            Thread.Sleep(Manager.SleepTime);
            Driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)=concat('We', \"'\", 'll display this link to anyone who visits the deleted repository.')])[1]/following::button[1]")).Click();
            Thread.Sleep(Manager.SleepTime);
        }

        internal RepHelper(ApplicationManager manager) : base(manager)
        {

        }
    }
}
