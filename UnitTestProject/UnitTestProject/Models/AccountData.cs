﻿using NUnit.Framework.Constraints;

namespace UnitTestProject.Models
{
    public class AccountData
    {
        public string Login { get; set; }
        public  string Pass { get; set; }
        public string UserName { get; set; }

        public AccountData(string login, string pass, string shName)
        {
            Login = login;
            Pass = pass;
            UserName = shName;
        }

        internal static AccountData GetUser() => new AccountData(Settings.Login,Settings.Pass,Settings.UserName);
    }
}
