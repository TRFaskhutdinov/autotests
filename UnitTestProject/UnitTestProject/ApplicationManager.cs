﻿using System;
using System.Text;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using UnitTestProject.Helpers;
using static UnitTestProject.Settings;

namespace UnitTestProject
{
    internal class ApplicationManager
    {
        private bool _acceptNextAlert = true;

        private static  ThreadLocal<ApplicationManager> _app=new ThreadLocal<ApplicationManager>();

        internal LoginHelper Auth { get;}

        internal NavigationHelper Navigation { get;}

        internal RepHelper Rep { get;}
        public IWebDriver Driver { get;}
        public int SleepTime { get; }

        internal StringBuilder VerificationErrors { get; }
        public string BaseUrl { get;}

        internal ApplicationManager()
        {
            Driver=new FirefoxDriver(@"C:\Source\Tests\autotests\UnitTestProject");
            Driver.Manage().Window.Maximize();
            //BaseUrl = "https://bitbucket.org";
            BaseUrl = Settings.BaseUrl;
            VerificationErrors=new StringBuilder();
            Navigation=new NavigationHelper(this);
            Auth=new LoginHelper(this);
            Rep=new RepHelper(this);
            SleepTime = 500;
        }

        internal static ApplicationManager GetInstance()
        {
            if (!_app.IsValueCreated)
            {
                ApplicationManager newInst=new ApplicationManager();
                newInst.Navigation.OpenHomePage();
                Thread.Sleep(newInst.SleepTime);
                _app.Value = newInst;
            }

            return _app.Value;
        }

        ~ApplicationManager()
        {
            try
            {
                Auth.LogOut();
                Driver.Quit();
            }
            catch (Exception)
            {
                // Ignore errors if unable to close the browser
            }
            Assert.AreEqual("", VerificationErrors.ToString());
        }
    }
}
