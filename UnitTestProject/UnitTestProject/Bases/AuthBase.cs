﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;
using UnitTestProject.Models;

namespace UnitTestProject.Bases
{
    internal abstract class AuthBase : TestBase
    {
        protected AccountData User;

        [SetUp]
        public new void SetupTest()
        {
            //User = new AccountData("TestLoginName", "TestPass", "UserName"); // I change to generic data
            User = AccountData.GetUser();
            Manager.Navigation.OpenHomePage();
            Manager.Auth.LogIn(User);
        }

        //[TearDown]
        //public new void TeardownTest()
        //{
        //    try
        //    {
        //        Manager.Auth.LogOut();
        //        Manager.Stop();
        //    }
        //    catch (Exception)
        //    {
        //        // Ignore errors if unable to close the browser
        //    }
        //    Assert.AreEqual("", Manager.VerificationErrors.ToString());
        //}
    }
}
