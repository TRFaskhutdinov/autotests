﻿using OpenQA.Selenium;

namespace UnitTestProject.Bases
{
    internal abstract class HelperBase
    {
        protected bool AcceptNextAlert = true;
        protected ApplicationManager Manager;
        protected IWebDriver Driver;

        internal HelperBase(ApplicationManager manager)
        {
            Manager = manager;
            Driver = manager.Driver;
        }

        internal bool IsElementPresent(By by)
        {
            try
            {
                Driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        internal bool IsAlertPresent()
        {
            try
            {
                Driver.SwitchTo().Alert();
                return true;
            }
            catch (NoAlertPresentException)
            {
                return false;
            }
        }

        internal string CloseAlertAndGetItsText()
        {
            try
            {
                IAlert alert = Driver.SwitchTo().Alert();
                string alertText = alert.Text;
                if (AcceptNextAlert)
                {
                    alert.Accept();
                }
                else
                {
                    alert.Dismiss();
                }
                return alertText;
            }
            finally
            {
                AcceptNextAlert = true;
            }
        }
    }
}
