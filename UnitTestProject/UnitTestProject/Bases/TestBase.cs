﻿using System;
using NUnit.Framework;
using UnitTestProject.Models;

namespace UnitTestProject.Bases
{
    internal abstract class TestBase
    {
        protected ApplicationManager Manager;


        [SetUp]
        public void SetupTest()
        {
            Manager = ApplicationManager.GetInstance();
            Manager.Navigation.OpenHomePage();
        }

        //[TearDown]
        //public void TeardownTest()
        //{
        //    try
        //    {
        //        Manager.Stop();
        //    }
        //    catch (Exception)
        //    {
        //        // Ignore errors if unable to close the browser
        //    }
        //    Assert.AreEqual("", Manager.VerificationErrors.ToString());
        //}
    }
}
