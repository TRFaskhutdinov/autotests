using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Xml.Serialization;
using NUnit.Framework;
using OpenQA.Selenium;
using UnitTestProject.Bases;
using UnitTestProject.Models;

namespace UnitTestProject.Tests
{
    [TestFixture]
    internal class RepTests : AuthBase
    {
        public static IEnumerable<RepData> RepDataFromXmlFile()
        {
            return (List<RepData>)new XmlSerializer(typeof(List<RepData>)).Deserialize(new StreamReader(@"C:\Source\Tests\autotests\UnitTestProject\UnitTestProject\bin\Debug\Data.xml"));

        }

        [Test, TestCaseSource("RepDataFromXmlFile")]
        public void CreateDeleteRepTest(RepData rep)
        {
                Manager.Rep.CreateRep(rep);
                Assert.IsTrue(Manager.Driver.Url.Contains(rep.Name.ToLower()));
                Manager.Rep.DeleteRep();
                Thread.Sleep(1000);
                Manager.Navigation.OpenRepListPage();
                bool f = false;
                try
                {
                    Manager.Driver.FindElement(By.LinkText(rep.Name));
                }
                catch (NoSuchElementException)
                {
                    f = true;
                }
                Assert.IsTrue(f);
        }
    }
}
