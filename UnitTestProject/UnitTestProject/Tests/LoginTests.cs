﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using UnitTestProject.Bases;
using UnitTestProject.Models;

namespace UnitTestProject.Tests
{
    [TestFixture]
    internal class LoginTests : TestBase
    {
        internal static AccountData User = AccountData.GetUser();

        [Test]
        public void LoginLogoutValidTest()
        {          
            Manager.Auth.LogIn(User);
            Assert.IsTrue(Manager.Auth.IsLoggedIn(User.UserName));
            Manager.Auth.LogOut();
            Assert.IsFalse(Manager.Auth.IsLoggedIn());
        }

        [Test]
        public void LoginInvalidTest()
        {
            var fakeUser = new AccountData("jfighdrgh@mail.com", "Ы", User.UserName);
            Manager.Auth.LogIn(fakeUser);
            Assert.IsFalse(Manager.Auth.IsLoggedIn());
        }
    }
}
