﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using AutoIt;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using VirtualBoxTests.Bases;
using VirtualBoxTests.Helpers;
using VirtualBoxTests.Models;

namespace VirtualBoxTests.Tests
{
    [TestFixture]
    public class PaintTests : TestBase
    {
        public static IEnumerable<StringModel> StringsFromXmlFile()
        {
            return (List<StringModel>)new XmlSerializer(typeof(List<StringModel>)).Deserialize(new StreamReader(@"C:\Source\Tests\autotests\VirtualBoxTests\VirtualBoxTests\bin\Debug\Data.xml"));

        }

        [Test, TestCaseSource("StringsFromXmlFile")]
        public void PaintString(StringModel s)
        {
            Manager.Paint.PaintAString(s);
            AutoItX.Sleep(2000);
        }
    }
}
