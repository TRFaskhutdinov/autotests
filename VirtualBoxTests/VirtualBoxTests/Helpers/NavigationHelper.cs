﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoIt;
using VirtualBoxTests.Bases;

namespace VirtualBoxTests.Helpers
{
    class NavigationHelper : HelperBase
    {
        internal void NewFile()
        {
            AutoItX.Send("^n{RIGHT}{ENTER}");
        }

        internal void Quit()
        {
            AutoItX.Send("!{F4}{RIGHT}{ENTER}");
        }

        internal  IntPtr GetMainWindow() => AutoItX.WinGetHandle("[CLASS:MSPaintApp]");

        internal NavigationHelper(ApplicationManager manager) : base(manager)
        {

        }
    }
}
