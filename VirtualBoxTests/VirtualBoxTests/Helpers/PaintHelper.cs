﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoIt;
using VirtualBoxTests.Bases;
using VirtualBoxTests.Models;

namespace VirtualBoxTests.Helpers
{
    class PaintHelper : HelperBase
    {
        internal void PaintAString(StringModel s)
        {
            AutoItX.MouseClickDrag("LEFT", s.X1, s.Y1, s.X2, s.Y2);
        }

        internal PaintHelper(ApplicationManager manager) : base(manager)
        {

        }
    }
}
