﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutoIt;
using NUnit.Framework;
using VirtualBoxTests.Helpers;

namespace VirtualBoxTests
{
    public class ApplicationManager
    {
        private bool _acceptNextAlert = true;

        private static ThreadLocal<ApplicationManager> _app = new ThreadLocal<ApplicationManager>();

        internal NavigationHelper Navigation { get; }

        internal PaintHelper Paint { get; }

        internal StringBuilder VerificationErrors { get; }

        internal ApplicationManager()
        {
            VerificationErrors = new StringBuilder();
            Navigation = new NavigationHelper(this);
            Paint=new PaintHelper(this);
            AutoItX.Run("mspaint.exe", "", AutoItX.SW_SHOW);
            AutoItX.Sleep(2000);
        }

        internal static ApplicationManager GetInstance()
        {
            if (!_app.IsValueCreated)
            {
                ApplicationManager newInst = new ApplicationManager();
                _app.Value = newInst;
            }

            return _app.Value;
        }

        ~ApplicationManager()
        {
            try
            {
                Navigation.Quit();
            }
            catch (Exception)
            {
                // Ignore errors if unable to close the browser
            }
            Assert.AreEqual("", VerificationErrors.ToString());
        }
    }
}
