﻿using AutoIt;
using NUnit.Framework;

namespace VirtualBoxTests.Bases
{
    public class TestBase
    {
        protected ApplicationManager Manager;


        [SetUp]
        public void SetupTest()
        {
            Manager = ApplicationManager.GetInstance();
            Manager.Navigation.NewFile();
            AutoItX.WinActivate(Manager.Navigation.GetMainWindow());
            NUnit.Framework.Assert.IsFalse(AutoItX.WinActive(Manager.Navigation.GetMainWindow()) == 0);
            AutoItX.WinSetState(Manager.Navigation.GetMainWindow(), AutoItX.SW_MAXIMIZE);
            AutoItX.AutoItSetOption("SendKeyDelay", 400);
        }
    }
}
