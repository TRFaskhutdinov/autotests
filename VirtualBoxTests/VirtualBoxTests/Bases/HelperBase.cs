﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualBoxTests.Bases
{
    class HelperBase
    {
        protected bool AcceptNextAlert = true;
        protected ApplicationManager Manager;

        internal HelperBase(ApplicationManager manager)
        {
            Manager = manager;
        }
    }
}
