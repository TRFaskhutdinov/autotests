﻿using System;
using AutoIt;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VirtualBoxTests.Bases;
using VirtualBoxTests.Models;

namespace VirtualBoxTests
{
    [TestClass]
    internal class UnitTest1 : TestBase
    {
        [TestMethod]
        public void TestMethod1()
        {
            Manager.Paint.PaintAString(new StringModel(200, 200, 500, 500));
            AutoItX.Sleep(2000);
        }
    }
}
